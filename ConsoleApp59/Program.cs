﻿using System;

namespace ConsoleApp59
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = new int[5, 5];

            int n = arr.GetLength(0);
            int m = arr.GetLength(1);

            #region Create And Print Random Array

            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    arr[i, j] = rnd.Next(10, 100);
                    Console.Write(arr[i, j] + "  ");
                    //Console.WriteLine($"[{i},{j}] = {arr[i,j]}");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            #endregion

            int max = arr[0, 0];
            for (int i = 0; i < n; i++)
            {
                {
                    if (max < arr[i, i])
                    {
                        max = arr[i, i];
                    }
                }
            }
            Console.WriteLine("Max Number of General Diaganal=" + max);
            Console.ReadLine();

        }
    }
}
